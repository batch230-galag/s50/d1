import React from 'react'

// >> Creates a Context Object
// A context object with object data type that can be used to store information that can be shared to other components within the application
const UserContext = React.createContext()
console.log(UserContext)

// The "Provider" component allows other components to consume/use the context objects and supply the necessary information needed in the context object
// The Provide is used to create a context that can be consumed.


export const UserProvider = UserContext.Provider

export default UserContext;