import './App.css';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Routes } from 'react-router-dom';

// React fragments allows us to return multiple elements
// import { Fragment } from 'react';
import { useState } from 'react';

// s54
import { UserProvider } from './UserContext';

// Pages
import AppNavBar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import PageNotFound from './pages/PageNotFound'
import Logout from './pages/Logout';

// s56
import CourseView from './pages/CourseView';

// s55
import { useEffect, useContext } from 'react';

// Additional Example
import Settings from './pages/Settings';

export default function App() {
  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    id: localStorage.getItem('id'),
    isAdmin: localStorage.getItem('isAdmin')
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
      <Container fluid>
        <AppNavBar />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/courses' element={<Courses />} />
            <Route path='/courses/:courseId' element={<CourseView />} />
            <Route path='/register' element={<Register />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/settings' element={<Settings />} />
            <Route path='*' element={<PageNotFound />} />
          </Routes>
      </Container>
      </Router>
    </UserProvider>
  );
}

/* 
  <Router>
    <Routes>
      <Route path="/yourDesiredPath" element = { <Component /> } />
    </Routes>
  </Router>
*/

// export default App;
