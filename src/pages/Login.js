import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

import Swal from 'sweetalert2';


export default function Login(){
    
    // variable, setter function
    const {user, setUser} = useContext(UserContext)

    // State hooks to store the values of input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // Check if values are successfully binded
    console.log(email)
    console.log(password);

    console.log(`${email} has been successfully logged in`)

/* document.querySelector('#email').addEventListener('keyup', (e) => {
setEmail(e.target.value)
}) */

    /* function authenticate(e) {
        // prevents page redirection via form submission
        e.preventDefault();

        setUser({
            email: localStorage.setItem('email', email)
        })
        
        // Clear input field
        setEmail('');
        setPassword1('');
        alert('Login Successful!')
        window.location.reload()
    } */

    function authenticate(e) {
        // prevents page redirection via form submission
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken")
            console.log(data.accessToken)
            
            if(typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken);
                
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })

                setEmail('')
                setPassword('')

                // window.location.replace("/courses")

            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check you login details and try again."
                })
            }

        })

        // alert('Login Successful!')

    }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            localStorage.setItem('email', data.email)
            localStorage.setItem('_id', data._id)
            localStorage.setItem('isAdmin', data.isAdmin)

            setUser({
                email: data.email,
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '') ) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        (user.id !== null) 
        ? // true - means email field is successfully set
            <Navigate to="/courses"/>
        : // false - means email field is not successfully set
        <Form onSubmit={(e) => authenticate(e)}>
        <h3>Login</h3>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)} 
                    required
                />
            </Form.Group>
            { isActive ? 
                <Button variant="success" type="submit" id="submitBtn" >
                    Login
                </Button>
            :
                <Button variant="secondary" type="submit" id="submitBtn" disabled>
                    Please Input Field First
                </Button>
            }
        </Form>
        

    )

}