import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";


export default function Register(){
    const { user } = useContext(UserContext)
    
    // State hooks to store the values of input fields
    const [fName, setFname] = useState('');
    const [lName, setLname] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const navigate = useNavigate()

    // // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    /* // Check if values are successfully binded
    console.log(email)
    console.log(password1);
    console.log(password2);
 */
    /* document.querySelector('#email').addEventListener('keyup', (e) => {
        setEmail(e.target.value)
    }) */

    function registerUser(event) {
        // prevents page redirection via form submission
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1,
                firstName: fName,
                lastName: lName,
                mobileNumber: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            
            if(data) {

                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })

                setEmail('');
                setPassword1('');
                setPassword2('');
                navigate("/login")

                // window.location.replace("/courses")

            } else {
                Swal.fire({
                    title: "Registration Failed",
                    icon: "error",
                    text: "Please Input the missing fields/Duplicate email."
                })
            }

        })
        

        // alert('Login Successful!')

        // Clear input field
        // alert('Thank you for registering!')
    }

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password1, password2])

    return(
        (user.id !== null)
        ?
            <Navigate to="/courses" />
        :
        <Form onSubmit={(event) => registerUser(event)}>
            <h1>Register</h1>
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="First Name"
                    value={fName}
                    onChange={e => setFname(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>
            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Last Name"
                    value={lName}
                    onChange={e => setLname(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)} 
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)} 
                    required
                />
            </Form.Group>
            
            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Mobile Number"
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)} 
                    required
                />
            </Form.Group>
            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn" >
                    Register
                </Button>
            :
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Register
                </Button>
            }
        </Form>

    )

}