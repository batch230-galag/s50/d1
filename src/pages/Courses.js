import { Fragment, useContext, useEffect, useState } from 'react';
// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import UserContext from '../UserContext';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';

export default function Courses(){
	const { user } = useContext(UserContext)
	const navigate = useNavigate()

/* 	console.log("Contents of coursesData: ");
	console.log(coursesData);
	console.log(coursesData[0]);
 */
	
	
	// array method to display all courses
	/* const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} /> 
		)
	}) */
	// return(
	// 	<Fragment>
	// 		<CourseCard courseProp={coursesData[0]} />
	// 	</Fragment>
	// )

	const [courses, setCourses] = useState([])

	const goTo = () => {
		navigate("/")
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />  
				)
			}))
		})
	}, [])

	return (
		(user.isAdmin)
		?
		<>
		<div>No Courses Added</div>
		<Button variant="success" type="submit" id="submitBtn" onClick={goTo}>
		Add Course
		</Button>
		</>
		:
			<Fragment>
			<h1>Courses</h1>
				{courses}
			</Fragment>
		
		
	)
}