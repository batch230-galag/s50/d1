import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Logout(e) {

    const { unsetUser, setUser } = useContext(UserContext)

    Swal.fire({
        title: "You have been logout",
        icon: "warning",
        timer: 2000
    })

    unsetUser();

    useEffect(() => {
        setUser({
            email: null,
            id: null})
    })


    return(
        <Navigate to="/" />
    )
}