import { Fragment } from "react";
import Banner from '../components/Banner';
import Highlights from '../components/Highlight';


export default function home() {
    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere",
        destination: "/",
        label: "Enroll Now"
    }


    return(
        <Fragment>
            <Banner data={data}></Banner>
            <Highlights />
        </Fragment>
    )
}