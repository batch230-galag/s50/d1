import { Fragment } from "react";
import { Link } from "react-router-dom";
import Banner from "../components/Banner";

export default function PageNotFound() {
    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Go back"
    }


    return(
        <Fragment>
            <Banner data={data}></Banner>
        </Fragment>
    )

    /* return(
        <Fragment>
            <h1>404 PAGE NOT FOUND</h1>
            <p>Go back to <Link to="/">Homepage</Link></p>
        </Fragment>
    ) */
}