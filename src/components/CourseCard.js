import { Card, Button } from "react-bootstrap";
import PropTypes from 'prop-types'
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
// import CloseButton from 'react-bootstrap/CloseButton';

export default function CourseCard({courseProp}) {

    const {_id, name, description, price} = courseProp

    // State Hooks (useState) - a way to store information within a component and track this information
		// getter, setter
		// variable, function to change the value of a variable
        const [count, setCount] = useState(0); // count = 0;
        const [seatLeft, setSeats] = useState(10); // count = 0;
        const [disable, setDisable] = useState(false);


        
        function enroll(){
        /*     if(seatLeft > 0) {
                setSeats(seatLeft - 1)
                setCount(count + 1);
                console.log('Enrollees: ' + count);
            } else {
                alert("No more Seats")
                setDisable(true)
            } */

                // setSeats(seatLeft - 1)
                // setCount(count + 1);
                // console.log('Enrollees: ' + count);
                // console.log('setSeats: ' + seatLeft)
        }

        
        // useEffect() always run the task on the initial render and/or every render (when the state changes in a component)
        // Initial render is when the component is run or displayed for the time
       /*  useEffect(() => {
            if(seatLeft > 0) {
                setDisable(false)
            } else {
                alert('No more seats available')
                setDisable(true)
            }
        }, [seatLeft])  */

    
    return(
        <Card className="cardHighlight p-3">
        <Card.Body>
            <Card.Title>
                <h2>{name}</h2>
            </Card.Title>
            <Card.Text>
                Description:
            </Card.Text>
                <Card.Subtitle>
                    {description}
                </Card.Subtitle>
            <Card.Text>
                Price:
            </Card.Text>
                <Card.Subtitle>
                    {price}
                </Card.Subtitle>
            <Card.Text>Total Enrolled: {count}</Card.Text>
                <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
            {/* {disable ?
                <Button variant="primary" disabled onClick={enroll}>Enroll</Button>
            :
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            } */}
        </Card.Body>
    </Card>
    )
}

// Check if the CourseCard component is getting the correct property types
/* CourseCard.propTypes = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
} */
