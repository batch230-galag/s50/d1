import { Container, Navbar, Nav } from 'react-bootstrap'
import { Link, Navigate, NavLink } from 'react-router-dom'
import { Fragment, useContext, useState } from 'react'
import UserContext from '../UserContext'


export default function AppNavBar() {
	// s55
	const {user} = useContext(UserContext);
	console.log(user.email)
	console.log(user.id)
	// const [user, setUser] = useState(localStorage.getItem("email"))
	// console.log(userEmail);
	// console.log(userPassword);

	/* function logout() {
		window.location.reload()
		return(
			<Navigate replace to="/login" />
		)
	} */


    return(
        <Navbar collapseOnSelect bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/">Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
		        <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
		        <Navbar.Collapse id="responsive-navbar-nav">
		            <Nav className="mr-auto">
						<Nav.Link as={NavLink} to="/" href="#home">Home</Nav.Link>
						{(user.id !== null)?
							<Fragment>
								<Nav.Link as={NavLink} to="/courses" href="#courses">Courses</Nav.Link>
								<Nav.Link as={NavLink} to="/logout" href='#logout'>Logout {user.email}</Nav.Link>
							</Fragment>
						:
							<Fragment>
								<Nav.Link as={NavLink} to="/login" href='#login'>Login</Nav.Link>
								<Nav.Link as={NavLink} to="/register" href="#register">Register</Nav.Link>
							</Fragment>
						}
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
    )
}

// Link
// https://react-bootstrap.github.io/